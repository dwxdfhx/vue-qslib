

import baseComponent from './BaseQuestion';

export default  interface IQuestion {

    // 获取题的数据
    getData(): any;
    // 设置题目数据
    setData(d: string): void;
}
